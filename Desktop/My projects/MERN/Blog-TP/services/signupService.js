const signupModel = require('../models/signupModel');

const index = async ()=>{

  try {
    const data = await signupModel.find();
    return data;
  } catch (error) {
return error
  }

}

const find = async (id, res)=>{

  try {
    const data = await signupModel.findById(id);
    return data;
  } catch (error) {
return error
  }

}

const store = async (body)=>{

  try {

    const signupSchema = new signupModel(body);
    await signupSchema.save();
    return signupSchema;
  } catch (error) {
return error
  }

}

const update = async (id, body)=>{

  try {
    await signupModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
return error
  }

}

const destroy = async (id, res)=>{

  try {
    await signupModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

module.exports= {index, find, store, update, destroy}