import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom';
import { addSpaces } from './reducers/spaceReducer';

const SpaceCreate = () => {
  const [description, setDescription] = useState('')
  const [localization, setLocalization] = useState('')
  const [photo, setphoto] = useState('')



const spaces = useSelector((state) => state.spaces.spaces)
console.log(spaces)
const dispatch = useDispatch()
const Navigate = useNavigate()

const createSpace = (e) => {
e.preventDefault()
dispatch(addSpaces({id: Date.now(), description, localization, photo}))
Navigate('/space')
}


return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <h1>Space Information</h1>
    <form onSubmit={createSpace}>
      <div>
        <label htmlFor='description'>Description:</label>
        <input type='text' name='description' className='form-control' placeholder='Describe Your Engine' 
        onChange={e => setDescription(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='localization'>Localization:</label>
        <input type='text' name='localization' className='form-control' placeholder='Describe Your Engine' 
        onChange={e => setLocalization(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Add Photo' 
        onChange={e => setphoto(e.target.value)}/>
      </div>
      <button className='btn btn-info'>Submit</button>
    </form>
    </div>
    </div>
  );
}

export default SpaceCreate;
