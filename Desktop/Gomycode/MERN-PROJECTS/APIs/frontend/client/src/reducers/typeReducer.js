import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  types: [],
  type: {},
  
}

const typeSlice = createSlice({
  name: 'types',
  initialState,
  reducers: {
    addType: (state, action) => {
      
      state.types.push(action.payload)
    },

    updateType: (state, action) => {
const {id, description} = action.payload
      // state.user =action.payload
const eu = state.find(type => type.id === id)
if(eu) {
  eu.description = description

}
},

    deletetype: (state, action) => {
      const {id} = action.payload
    const eu = state.find(type => type.id === id)
    if(eu){
    return state.filter(f => f.id !== id)
    }
    }
  }
})

export const {addType, updateType, deletetype} = typeSlice.actions
export default typeSlice.reducer
