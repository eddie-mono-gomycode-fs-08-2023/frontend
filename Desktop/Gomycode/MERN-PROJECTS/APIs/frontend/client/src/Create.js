import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { addUser } from './reducers/userReducer';
import { useNavigate } from 'react-router-dom';

const Create = () => {
  const [name, setName] = useState('')
  const [surname, setSurname] = useState('')
  const [telephone, setTelephone] = useState('')
  const [photo, setPhoto] = useState('')

const users = useSelector((state) => state.users.users)
console.log(users)
const dispatch = useDispatch()
const Navigate = useNavigate()

const createUser = (e) => {
e.preventDefault()
dispatch(addUser({id: Date.now(), name, surname, telephone, photo }))
Navigate('/users')
}


return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <h1>Add A User</h1>
    <form onSubmit={createUser}>
      <div>
        <label htmlFor='name'>Name:</label>
        <input type='text' name='name' className='form-control' placeholder='Enter Name' 
        onChange={e => setName(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='surname'>Surname:</label>
        <input type='text' name='surname' className='form-control' placeholder='Enter Surname' 
        onChange={e => setSurname(e.target.value)}/>
      </div>

      <div>
        <label htmlFor='telephone'>Telephone:</label>
        <input type='phone' name='telephone' className='form-control' placeholder='Enter Surname' 
        onChange={e => setTelephone(e.target.value)}/>
      </div>

      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Upload Photo' 
        onChange={e => setPhoto(e.target.value)}/>
      </div><br/>
      <button className='btn btn-info'>Submit</button>
    </form>
    </div>
    </div>
  );
}

export default Create;
