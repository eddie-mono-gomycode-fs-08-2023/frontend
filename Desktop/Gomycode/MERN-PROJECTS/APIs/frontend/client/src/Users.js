import React from 'react';
import {useDispatch, useSelector,  } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteUser } from './reducers/userReducer';
import './Users.css'
import RecordDrop from './dropDown/RecordDrop';

const Users = () => {
  const users = useSelector((state) => state.users.users)
  console.log(users);
const dispatch = useDispatch()

const handleDelete = (id) => {
dispatch(deleteUser({id: id}))
}

  return (
    <>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
    {/* <Link to='/engine'>Record Engine</Link> */}

<RecordDrop/>
    {/* <Link to='/parking'>Parking Space</Link> */}
    {/* <Link to='/service'>Services</Link> */}
    {/* <Link to='/users'>Users</Link> */}
    <Link to='/'>Sign out</Link>
    </div>

    <div className='container'>
    <h3>Users List</h3>
    <Link to='/create' className='btn btn-success my-3'>Create +</Link>
    <table className='table'>
      <thead>
        <tr>
          <th>ID</th>
          <th>NAME</th>
          <th>SURNAME</th>
          <th>TELEPHONE</th>
          <th>PHOTO</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
{users.map((user, index) => {
  return(
    <tr key={index}>
  <td>{user.id}</td>
  
  <td>{user.name}</td>

  <td>{user.surname}</td>

  <td>{user.telephone}</td>
  
  <td>{user.photo}</td>
  <td>
<Link to={`/edit/${user.id}`} className='btn btn-sm btn-primary'>Edit</Link>
<button onClick={() => handleDelete(user.id)} className='btn btn-sm btn-danger'>Delete</button>
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>
        </>

  );
}

export default Users;
