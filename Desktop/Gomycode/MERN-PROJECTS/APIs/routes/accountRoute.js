var express = require('express');
var router = express.Router()
var accountController = require('../controllers/accountController')

router.get('/', accountController.find)

router.get('/:id', accountController.find)

router.post('/', accountController.store)

router.put('/:id', accountController.update)

router.delete('/:id', accountController.destroy)

module.exports = router;