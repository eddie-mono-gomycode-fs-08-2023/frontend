
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var cors = require('cors')
var PORT = 3300;




var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var companyRouter = require('./routes/companyRoute');
var accountRouter = require('./routes/accountRoute');
var engineRouter = require('./routes/engineRoute')
var spaceRouter = require('./routes/spaceRoute');
var typeRouter = require('./routes/typeRoute');
var userRouter = require('./routes/userRoute');



var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/company', companyRouter);
app.use('/accounts', accountRouter);
app.use('/engines', engineRouter);
app.use('/spaces', spaceRouter);
app.use('/types', typeRouter);
app.use('/user', userRouter);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(PORT, (req, res) => { 
  console.log('connected to server');
})

mongoose.connect('mongodb://127.0.0.1:27017/API')
// mongoose.connect('mongodb+srv://eddymono148:olokor148@cluster0.auedwxv.mongodb.net/API?retryWrites=true&w=majority')
  // catch(error => handleError(error));
  console.log('connected to db');

module.exports = app;
