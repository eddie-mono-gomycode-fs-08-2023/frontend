import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './Client.css'
import axios from 'axios';
import Navbari from './Navbari';

const Client = () => {
  const Navigate = useNavigate()
  const [users, setUsers] = useState([]);
  const [name, setName] = useState('')
  const [surname, setSurname] = useState('')
  const [telephone, setTelephone] = useState('')


  const handleSignIn = (e) => {
e.preventDefault()
  axios.post('http://localhost:3300/userss', {name, surname, telephone})
  .then(() => {
    setUsers({name, surname, telephone})
  })

  }


  return (
    <div>
    <Navbari/>
    <div className='Client-page1'>
<>
    <div className='Client-page'>
    
      <form>
        <input style={{width: '100%', backgroundColor: 'yellow'}} type='text' name='name' placeholder='Enter Name' onChange={(e) => {setName(e.target.value)}}/>
        <br/>
        <input style={{width: '100%'}} type='text' name='surname' placeholder='Enter Surname' onChange={(e) => {setSurname(e.target.value)}}/>
        <br/>
        <input style={{width: '100%'}} type='text' name='telephone' placeholder='Enter Phone Number' onChange={(e) => {setTelephone(e.target.value)}}/>
        {/* <input type='file' name='image' placeholder='Add an Image'/> */}
        <br/>
        <input className='client-btn' style={{width: '50%'}} type='submit' value='LOGIN' onClick={handleSignIn}/>
      </form>
      </div>
      </>
      </div>
      </div>

  );
}

export default Client;
