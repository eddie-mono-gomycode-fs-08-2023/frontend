import React from 'react';
import './Motorbike.css'
import SpaceNav from './SpaceNav';
import { useNavigate } from 'react-router-dom';


const Space = () => {
  const Navigate = useNavigate()
  return (
    <div>
    <SpaceNav/>
    <div className='HomeO'>
    <div className='Motorbike'>

        </div>
        <table>
          <thead>
          <th>Id</th>
            <th>description</th>
            <th>Address</th>
            <th>Photo</th>
            <th>Action</th>

            </thead>

            <tr>
              <td>01</td>
              <td>Garage</td>
              <td>Space 1</td>
              <td>{}</td>
              <button onClick={() => Navigate('/spaces')}>Book A Space</button>
              </tr>

              <tr>
              <td>02</td>
              <td>Garage</td>
              <td>Space 2</td>
              <td>{}</td>
              <button onClick={() => Navigate('/spaces')}>Book A Space</button>
              
              </tr>

          
        </table>
    
    </div>
    </div>

  );
}

export default Space;
