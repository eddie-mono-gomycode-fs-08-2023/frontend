import React from 'react';
import {Link, useNavigate} from 'react-router-dom';
// import {useNavigate} from 'react-router-dom';
import DropDown from './dropDown/DropDown';
import SpaceDrop from './dropDown/spaceDrop';
import SignupDrop from './dropDown/signupDrop';
import BarDrop from './dropDown/BarDrop';
import UserDrop from './dropDown/UserDrop';
import Table from './clientData/Table';

const SpaceNav = () => {
const Navigate = useNavigate()
return (
<div>
<>
<div className='Nav1'>
<span>
<Link to='/' className='Home-logo'>
Monopark
<img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGVafREVgyyBi_lOTgmTkls6U_mk3bcBlSiA&usqp=CAU' alt='Logo'></img>
</Link>
{/* <BarDrop/> */}
</span>
{/* <BarDrop/> */}

<nav>
<ul>
<SpaceDrop/>
{/* <a href='/motorbike'>moto</a> */}
{/* <a href='/space'>Space</a> */}
{/* <Link to='/users'>Users</Link> */}
{/* <Link to='/table'>Table</Link> */}
<Link to='/account'>Account</Link>
{/* <Link to='/company'>Company</Link> */}
{/* <Link to='/clprofile'>Profile</Link> */}
{/* <DropDown/> */}
<Link to='/'>Logout</Link>

{/* <SignupDrop/> */}

{/* For mobile */}
{/* <BarDrop/> */}
{/* <UserDrop/> */}

</ul>
</nav>
</div>

</>

</div>

);
}

export default SpaceNav;
