
const requestModel = require('../models/requestModel')



const index = async (res)=>{

  try {
    const data = await requestModel.find();
    return data;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const find = async (id, res)=>{


  try {
    const data = await requestModel.findById(id);
    return data;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const store = async (body, res)=>{

  try {

    const requestSchema = new requestModel(body);
    await requestSchema.save();
    return requestSchema;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const update = async (id, body, res)=>{

  try {
    await requestModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const destroy = async (id, res)=>{

  try {
    await requestModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}



// const all = async() => {
//   const types = await typeModel.findAll();
//   return types;
// };

module.exports= {index, find, store, update, destroy}