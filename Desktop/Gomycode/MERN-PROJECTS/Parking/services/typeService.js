const typeModel = require('../models/accountModel')



const index = async (res)=>{

  try {
    const data = await typeModel.find();
    return data;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const find = async (id, res)=>{


  try {
    const data = await typeModel.findById(id);
    return data;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const store = async (body, res)=>{

  try {

    const typeSchema = new typeModel(body);
    await typeSchema.save();
    return typeSchema;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const update = async (id, body, res)=>{

  try {
    await typeModel.findByIdAndUpdate(id, body);
    return true;
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}

const destroy = async (id, res)=>{

  try {
    await typeModel.findByIdAndDelete(id);
    return true
  } catch (error) {
    res.status(error.status).json({error: error.message});
  }

}



// const all = async() => {
//   const types = await typeModel.findAll();
//   return types;
// };

module.exports= {index, find, store, update, destroy}