import React, {useState, useEffect} from 'react';
import {Link, useNavigate} from 'react-router-dom';

import axios from 'axios'

//https://animalfoundation.com/

const Home = () => {
  const Navigate = useNavigate()
  const [blogs, setBlogs] = useState([])
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [age, setAge] = useState('')
  const [password, setPassword] = useState('')

  useEffect(() => {
    axios.get('http://localhost:3300')
    .then((response) => {
      setBlogs(response.data)
    })
}, []);

  return (
  
<div>
{/* {blogs.map((blog) => {
  return(
    <>
    <h1>{blog.name}</h1>
<h1>{blog.posts}</h1>
</>
  )
})}  */}

<div className='Home-menu' style={{display: 'flex', justifyContent: 'space-evenly', width: '100%', height: '70px', backgroundColor: '', borderBottom: '1px solid black', alignItems: 'center'}}>
<button onClick={() => Navigate('/dogs')} style={{border: 'none', backgroundColor: 'black', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}><b>Dogs</b></button>
<button onClick={() => Navigate('/cats')} style={{border: 'none', backgroundColor: 'black', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}><b>Cats</b></button>
<button onClick={() => Navigate('/birds')} style={{border: 'none', backgroundColor: 'black', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}><b>Birds</b></button>
{/* <Link to='/all'>All Blogs</Link> */}
<button onClick={() => Navigate('/blogs')} style={{border: 'none', backgroundColor: 'black', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}><b>All Blogs</b></button>
<button onClick={() => Navigate('/signup')} style={{border: 'none', backgroundColor: 'red', color: 'white', height: '30px', width: '6%', cursor: 'pointer'}}><b>Sign up</b></button> 
<Link to='/signup'>Sign  up</Link>

</div>

{/* Body container Grid */}
<div className='Grid-container' style={{backgroundColor: '', width: '100%', height: '500px'}}>
{/* Flex container */}
<div className='Flex-container' style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: '100px'}}>

{/* Left Side */}
<div style={{width: '45%', height: '450px', backgroundColor: ''}}>
<h1 style={{fontSize: '50px'}}>
  Our aim is to make your News and Information<br/> come to Live
  </h1>

  <p><h2>
    Share with us the recent happenings in your community.<br/>
Stop hesitating and make the world hear you out<br/>
</h2></p>
<h3>
Seize this opportunity, make your first post on our page</h3>
  
  <div>
  <button className='Post-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}} onClick={() => Navigate("/signup")}>Post A Blog</button>

</div>
</div>

{/* Right Side....List of Blogs container */}
<div style={{display: 'flex', justifyContent: 'space-evenly', width: '45%', borderLeft: '30px solid blue', borderTop: '30px solid blue', borderRadius: '25px', marginLeft: ''}}>
<div className='Space-1' style={{width: '33%', height: '400px', border: ''}}>
  <img src='https://news.stanford.edu/wp-content/uploads/2020/10/Birds_Culture-1-copy.jpg' alt='bird' style={{width: '100%', height: '220px'}}/>
  <p><h3><b>Birds Blog<br/>
Birds are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}}>Visit Blog</button>
</b></h3></p>
</div>
<div className='Space-2' style={{width: '33%', height: '400px', border: ''}}>
<img src='https://cdn.britannica.com/79/232779-050-6B0411D7/German-Shepherd-dog-Alsatian.jpg' alt='dog' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Dogs Blog<br/>
Dogs are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}}>Visit Blog</button>
</b></h3></p>
</div>
<div className='Space-3' style={{width: '34%', height: '400px', border: ''}}>
<img src='https://i.redd.it/isy3k3x008621.jpg' alt='cat' style={{width: '100%', height: '220px'}}/>
<p><h3><b>Cats Blog<br/>
Cats are really cute pets with a differ lifestyle<br/>
<button className='Pet-btn' style={{backgroundColor: 'yellow', color: 'black', borderRadius: '25px'}}>Visit Blog</button>
</b></h3></p>

</div>
</div>

</div>
</div>

</div>
    
    
  );
}


export default Home;

