const loginModel = require('../models/loginModel');
const signupModel = require('../models/signupModel');


const index = async() => {
try {
  const data = await loginModel.find();
  return data;
} catch (error) {
return data
}
};

const find = async(email) => {
try {
  const data = await loginModel.find({email})
  return data
} catch (error) {
return error
}
};

// const find = async() => {
//   try {
//     const data = await loginModel.find()
//     return data
//   } catch (error) {
//   return error
//   }
//   };

const store = async(body, res) => {
  const {email, password} = req.body
  try {
    const loginSchema = new signupModel.findOne({email, password});
    // await loginSchema.save();
    return loginSchema;
  } catch (error) {
    res.status(error.status).json({error: error.message})
  }
};

const update = async(id, body, res) => {
  try {
    await loginModel.findByIdAndUpdate(id, body)
    return true;
  } catch (error) {
    res.status(error.status).json({error: error.message})
  }
};

const destroy = async(id, res) => {
  try {
    await loginModel.findByIdAndDelete(id);
    return true;
  } catch (error) {
    res.status(error.status).json({error: error.message})
  }
};

module.exports = {index, find, store, update, destroy}