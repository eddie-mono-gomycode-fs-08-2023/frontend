// const express = require ('express');
const updateUserModel = require('../models/updateUserModel')
const updateUser = async(id, body, res) => {
try {
await updateUserModel.findByIdAndUpdate(id, body)
return true
} catch (error) {
  res.status(error.status).json({error: error.message})

}

}

module.exports = {updateUser}