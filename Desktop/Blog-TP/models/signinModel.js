const mongoose = require('mongoose');

const signinSchema = new mongoose.Schema({
  email:{
    type: String,
  },

  password: {
    type: String
  }
},
)

const Signin = mongoose.model('signin', signinSchema);
module.exports = Signin;