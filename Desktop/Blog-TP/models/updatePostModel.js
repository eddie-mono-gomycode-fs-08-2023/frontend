const mongoose =require('mongoose');

const updatePostSchema = new mongoose.Schema({
  name: {
    type: String
  },

  post: {
    type: String
  }
})

const UpdatePost = mongoose.model('updatePost', updatePostSchema);
module.exports = UpdatePost;